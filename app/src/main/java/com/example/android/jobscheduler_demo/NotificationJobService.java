package com.example.android.jobscheduler_demo;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;

/**
 * Created by parth on 16/3/18.
 */

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class NotificationJobService extends JobService {

    @Override
    public boolean onStartJob(JobParameters params) {

        PendingIntent contentPendingIntent=PendingIntent.getActivity(this,0,new Intent(this,
                MainActivity.class),PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationManager notificationManager= (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        NotificationCompat.Builder builder=new NotificationCompat.Builder(this,"1")
                .setContentTitle(getString(R.string.job_service)).setContentText(getString(R
                        .string.job_running)).setContentIntent(contentPendingIntent)
            .setSmallIcon(R.drawable.ic_job_running).setPriority(NotificationCompat
                        .PRIORITY_HIGH).setDefaults(NotificationCompat.DEFAULT_ALL).setAutoCancel
                        (true);
        notificationManager.notify(0,builder.build());

       return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }
}
